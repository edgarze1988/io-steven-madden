import style from './styles.css'

const videoNowServing = () => {
  return <>
            <div>
				<video controls muted playsInline={true} autoPlay={true} className={`${style.videoCapo}`}>
				       <source src='https://novedadescoliseum.com.pe/devs/archivos/sm-v2.mp4' type="video/mp4" />
				</video>		        
            </div>
        </>
}

export default videoNowServing;