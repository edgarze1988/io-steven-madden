import React from 'react'
import style from './styles.css'
import { useCssHandles } from 'vtex.css-handles'

const CSS_HANDLES = ['notfoundtitle1', 'background', 'text', 'item'] as const

const helloWorld = () => {
  const {handles} = useCssHandles(CSS_HANDLES);

  return <>
            <div className={handles.notfoundtitle1}>-</div>
            <div className={`${style.notfoundtitle2} dark-main`}>-</div>
            <a className="link--notfound-button" href="/">-</a>
        </>
}

export default helloWorld;
//<div className={handles.notfoundtitle}>No hay datos para esta búsqueda</div>
//crear un css con este nombre: merrellpe.store-theme-css ; stevemaddenpe.store-theme.css